#General Introduction of TBTtokencoin
TBTtoken Coin is a secure and energy efficient PoW/PoS coin. TBTtoken Coin uses a faster PoW distribution mechanism to distribute the initial coins, then after few days the coin is basically transferred to a pure PoS coin, where the generation of the coin is mainly through the PoS interests.

#TBTtoken Coin Specification

Maximum 28 million TBTTOKEN coin will ever exist to the community.

tbttoken coin adopt a variable PoS rate that will be given periodically payout as per following list,
- 1st 6 months -> 60% (10% per month)
- 2nd 6 months -> 50% (8% per month)
- 3rd 6 months -> 40% (7% per month)
- 4th 6 months -> 30% (5% per month)
- 5th 6 months -> 20% (3% per month)
- 6th 6 months -> 10% (1.4 per month and ongoing.)

#How are they used?
The TBTTOKEN coins are used by tbttoken community members for tbttoken services and to store and invest the wealth in a non-government controlled currency. The TBTTOKEN coins will also be used  as payment system on number of TBTTOKEN partner websites.

#How are they produced?
TBTTOKEN coin uses a special algorithm called the POW/POS to secure the TBTTOKEN Coin network. The moment you acquire TBTTOKEN Coin it becomes an interest bearing asset with 120% return per year through PoS minting. All you have to do to earn with this method is to hold coins in your TBTtoken-QT wallet. In addition to PoS minting, TBTTOKEN coin can be mined with CPU/GPU and does not need an ASIC miner like Bitcoin does.


